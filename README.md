# TemperoDaFe - Projeto Flask com Docker

Este é um projeto Flask chamado TemperoDaFe, que é uma página web que utilizamos para aprimorar nossos conhecimentos em Docker. Este README fornece instruções para configurar e executar o projeto.

## Pré-requisitos

Certifique-se de que você tenha os seguintes pré-requisitos instalados em sua máquina:

- Wsl: [Instalação do WSL](https://learn.microsoft.com/pt-br/windows/wsl/install)
- Docker: [Instalação do Docker](https://docs.docker.com/get-docker/)

## Configuração

 Clone este repositório para a sua máquina local:

   ```bash
   git clone https://gitlab.com/impdev/ac2.git
   cd ac2
   ```



## Executando o aplicativo com Docker 

Execute o seguinte comando para iniciar o aplicativo usando Docker:

```bash
docker build -t ac2 .
docker run -dp 5000:5000 ac2
```

Isso irá construir e iniciar o contêiner Flask.
 O aplicativo estará disponível em [http://localhost:5000](http://localhost:5000).

Para parar o aplicativo, você pode usar o seguinte comando, use o container ls para pegar o Id do container,
depois utilize o container stop para parar o docker:

```bash
docker container ls
docker container stop <IdContainer>
```

## Utilização

Acesse o aplicativo em [http://localhost:5000](http://localhost:5000) no seu navegador. Você pode se registrar, fazer login.

## Contribuição

Integrantes:


      Nome: Hygor Rodrigues Cesconetto
      RA : 2203138
 
      Nome : Pedro Luis Da Paz Dos Santos
      RA: 2202636

      Nome Ewerton Ferreira Costa
      RA : 2202829

      Nome : Nicole Santos Matos
      RA : 2202409
    
      Nome : João Victor Machado Maniezzo
      RA : 2202323
